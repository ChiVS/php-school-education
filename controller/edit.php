<?php

include_once __DIR__."/../model/Article.php";
date_default_timezone_set("Europe/Kiev");

if (isset($_POST['id']) && isset($_POST['name']) && isset($_POST['description']) && isset($_POST['created_at']))
    {
        $article = new Article();
        $article->save($_POST['name'], $_POST['description'], $_POST['created_at'], $_POST['id']);

        header('Location: ../controller/read.php');
    }

$id = $_GET['id'];
$row = (new Article())->findById($id);

require_once __DIR__."/../view/editTemplate.php";
?>