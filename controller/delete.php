<style>
    .btn {
        display: inline-block;
        background: Gray;
        color: #fff;
        padding: 0.5rem 0.5rem;
        text-decoration: none;
        border-radius: 3px;
    }
</style>

<?php

if (isset($_POST['NO']))
{
    header('Location: ../controller/read.php');
}

if (isset($_POST['YES'])) {

    include_once __DIR__ . "/../model/Article.php";
    date_default_timezone_set("Europe/Kiev");

    $id = $_GET['id'];
    $article = new Article();
    $article->deleteById($id);

    header('Location: ../controller/read.php');
}
?>

<p>THIS DATA WILL BE DELETED</p>

<form method="post">
    <input class="btn" type="submit" name="YES" value="YES">
    <input class="btn" type="submit" name="NO" value="NO">
</form>
