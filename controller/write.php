<?php

include_once __DIR__."/../model/Article.php";
date_default_timezone_set("Europe/Kiev");

$id = $_GET['id'];

if (isset($_POST['name']) && isset($_POST['description']) && isset($_POST['created_at']))
{
    $article = new Article();
    $article->save($_POST['name'], $_POST['description'], $_POST['created_at']);

    header('Location: ../controller/read.php');
}

// if ($id == NULL) {require_once __DIR__."/../view/editTemplate.php";}
// else {
    $row = (new Article())->findById($id);
    require_once __DIR__."/../view/editTemplate.php";
//    }
?>
