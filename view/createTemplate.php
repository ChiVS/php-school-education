<?php
$timestamp = date('Y-m-d H:i:s', time());
?>

<style>
    .btn {
        display: inline-block;
        background: Gray;
        color: #fff;
        padding: 0.5rem 0.5rem;
        text-decoration: none;
        border-radius: 3px;
    }
</style>

<form method="post" action="../controller/write.php">
    <input type="text" placeholder="name" name="name">
    <br>
    <input type="text" placeholder="description" name="description">
    <br>
    <input type="text" placeholder="created_at" name="created_at" value="<?php echo $timestamp ?>">
    <br>
    <input class="btn" type="submit" value="SUBMIT">
</form>
