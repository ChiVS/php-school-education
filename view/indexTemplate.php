<style>
    .btn {
        display: inline-block;
        background: Gray;
        color: #fff;
        padding: 0.5rem 0.5rem;
        text-decoration: none;
        border-radius: 3px;
    }
</style>

<table width="50%">
    <thead>
      <tr>
        <th>Name</th>
        <th>Description</th>
        <th>Created At</th>
        <th></th>
        <th></th>
      </tr>
    </thead>


    <?php

        foreach ($rows as $el)
        {
            echo "<td>" . $el['name'] . "</td><td>" . $el['description'] . "</td><td>" . $el['created_at'] . "</td><td>" . "</td><td>" . '<a href="../controller/edit.php?id='.$el['id'].'" class="btn">UPDATE</a>' .  "</td><td>" . '<a href="../controller/delete.php?id='.$el['id'].'" class="btn">DELETE</a><br>' . "</td></tr>";
        }
    ?>

</table>

<a href="../controller/create.php" class="btn">CREATE NEW DATA</a>
