<?php
$timestamp = date('Y-m-d H:i:s', time());
?>

<style>
    .btn {
        display: inline-block;
        background: Gray;
        color: #fff;
        padding: 0.5rem 0.5rem;
        text-decoration: none;
        border-radius: 3px;
    }
</style>

<form method="post" action="">
    <input type="text" placeholder="name" required name="name" value="<?php echo $row['name'] ?>">
    <br>
    <input type="text" placeholder="description" required name="description" value="<?php echo $row['description'] ?>">
    <br>
    <input type="text" placeholder="created_at" name="created_at" value="<?php echo $timestamp ?>">
    <input type="hidden" name="id" value="<?php echo $id ?>">
    <br>
    <input class="btn" type="submit" value="SUBMIT">
</form>
